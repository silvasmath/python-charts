import matplotlib.pyplot as plt
import numpy as np

import gi
gi.require_version('Gtk', '3.0')

plt.style.use('_mpl-gallery-nogrid')

# simple pie charts
labels = ['Renda Fixa', 'Ações', 'FIIs']
sizes = [35, 33, 32]

fig, ax = plt.subplots(figsize=(16, 9))

patches, texts = ax.pie(sizes, labels=labels, startangle=90,
                        colors=['#111e21ff', '#1b3b3cff', '#eecf99ff'])

for text in texts:
    text.set_fontsize(24)

# plt.save()
plt.savefig('simple_pie_chart.png', format='png', transparent=True, dpi=180)

# clear axes
ax.cla()

# pie charts
labels = ['Renda Fixa', 'Ações', 'FIIs']
sizes = [35, 33, 32]

fig, ax = plt.subplots(figsize=(16, 9))

patches, texts, autotexts = ax.pie(sizes, labels=labels, autopct='%1.1f%%', startangle=90,
                                   counterclock=False, colors=['#111e21ff', '#1b3b3cff', '#eecf99ff'])

for text in texts:
    text.set_fontsize(24)

for autotext in autotexts:
    autotext.set_color('white')
    autotext.set_fontsize(18)

# plt.save()
plt.savefig('ordered_pie_chart.png', format='png', transparent=True, dpi=180)

# clear axes
ax.cla()

fig, ax = plt.subplots(figsize=(16, 9))

ax.labelsize = 'large'

# horizontal bars chart
labels = ['Renda Fixa', 'Ações', 'FIIs']
sizes = [35, 33, 32]

ax.barh(labels, sizes, align='edge', color='#111e21ff')
plt.savefig('barh_chart.svg', format='svg', transparent=True, dpi=180)
